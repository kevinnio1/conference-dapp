const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const blockchainService = require('./services/blockchain.service.js');
const accountsRoute = require('./routes/accounts');
const conferenceRoute = require('./routes/conference');

const port = process.env.port || 3100;

app.use(bodyParser.json());

app.use('/api/accounts', accountsRoute);
app.use('/api/conference', conferenceRoute);


app.get('/', (req, res) => {
    res.send('Service is running');
});

app.listen(port, () => {
    console.log('started on port: ' + port);
    blockchainService.init();
});
