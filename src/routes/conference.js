const express = require('express');
const router = express.Router();
const blockchainService = require('../services/blockchain.service.js');
const bodyParser = require('body-parser');


router.get('/ticket/buy/:name', function (req, res) {
    blockchainService.buyTicket(req.params.name).then(function (result) {
        res.json(result);
    });
});

router.get('/ticket/price/:name', function (req, res) {
    blockchainService.getPrice(req.params.name).then(function (result) {
        res.json(result);
    });
});

module.exports = router;