pragma solidity >=0.4.21 <0.6.0;

contract Ownable {
    address public organizer;
    mapping (address => bool) public isAdmin; 
    event AddAdmin(address _admin, uint256 _timestamp);


    modifier onlyAdminOrOrganizer() {
        require(isAdmin[msg.sender] || msg.sender == organizer);
        _; 
    }
  
    constructor() public {
        organizer=msg.sender;
    }

    function addAdmin(address _admin) public returns (bool) {
        require(msg.sender == organizer);
        require(address(_admin) != address(0));
        require(!isAdmin[_admin]);
        isAdmin[_admin] = true;
        emit AddAdmin(_admin, now);
        return true;
    }
}