const Web3 = require('web3');
let web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:7545"));
var account;
// Step 1: Get a contract into my application
var json = require("../../truffle/build/contracts/Conference.json");
// Step 2: Turn that contract into an abstraction I can use
var contract = require("truffle-contract");
var Conference;
class CustomTransaction {
    constructor(hash, send) {
        this.hash = hash;
        this.send = send;
    }
}


module.exports = {
    init: function () {
        Conference = contract(json);
        // Step 3: Provision the contract with a web3 provider
        Conference.setProvider(web3.currentProvider);
        web3.eth.getAccounts(function (err, accs) {
            if (err != null) {
                console.log('There was an error fetching your accounts.' + err);
            } else if (accs.length == 0) {
                console.log('Couldn\'t get any accounts! Make sure your Ethereum client is configured correctly. Or make some accounts on your node.');
            } else {
                account = accs[0];
                console.log('Main account for sending transactions is : ' + account);
            }
        });
        this.watchEvents();
    },
    watchEvents: function () {
        var events;

        Conference.deployed().then(function (instance) {
            events = instance.allEvents();
        }).then(function () {
            events.watch(function (error, result) {
                if (!error) {
                    console.log(result);
                }
            });
        });

    }
    ,
    getAccounts: function () {
        return web3.eth.accounts;
    },
    buyTicket: function (name) {
        var meta;
        return Conference.deployed().then(function (instance) {
            meta = instance;
            transactionhash = meta.contract.buyTicket(name, { from: account, value: web3.toWei('5', 'ether') });
            ct = new CustomTransaction(transactionhash, false);
            return transactionhash;
        }).then(function (result) {
            console.log('buyticket succesfull: ' + result);
            return result;
        }).catch(function (err) {
            console.log('Something went wrong during buying a ticket' + err);
            return 'Something went wrong during buying a ticket: ' + err;
        });
    },
    getPrice: function () {
        var meta;
        return Conference.deployed().then(function (instance) {
            meta = instance;
            return meta.getPrice(account);
        }).then(function (result) {
            console.log('Price for account ' + account + ' is: ' + result);
            return result;
        }).catch(function (err) {
            console.log('something went wrong during getPrice: ' + err);
            return ('something went wrong during getPrice: ' + err);
        });
    }
}  