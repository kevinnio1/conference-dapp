const express = require('express');
const router = express.Router();
const blockchainService = require('../services/blockchain.service.js');


router.get('/', function (req, res) {
    res.json(blockchainService.getAccounts())
});

module.exports = router;