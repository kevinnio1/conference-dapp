pragma solidity >=0.4.21 <0.6.0;

import "./Ownable.sol";

contract Conference is Ownable{

    mapping(address => Registrant) public registrantsPaid;

    uint public numRegistrants;	
    uint public quota;

    event Deposit(address _from, uint _amount, string _name); 
    event Refund(address _to, uint _amount, string _name);
    event AlreadyRefunded(address _to, string _name);

    struct Registrant {
		uint256 price;
		string name;
    }

    constructor() public { 
        quota = 500;
        numRegistrants = 0;
    }

    function getPrice(address _recipient) public view returns (uint price) {
        return registrantsPaid[_recipient].price;
    }

    function getName(address _recipient) public view returns (string memory name) {
        return registrantsPaid[_recipient].name;
    }


    function buyTicket(string memory _name) public payable returns (bool success) { 
        require(numRegistrants < quota);
        Registrant storage reg = registrantsPaid[msg.sender];
        reg.price = msg.value;
        reg.name = _name;
        numRegistrants++;
        emit Deposit(msg.sender, msg.value, _name);
        return true;
    }
  
    function refundTicket(address payable _recipient) onlyAdminOrOrganizer public {
        Registrant storage reg = registrantsPaid[_recipient];
    
        uint amount = reg.price;
    
        if (amount == 0) {
            emit AlreadyRefunded(_recipient, reg.name);
            revert();
        } else if (address(this).balance >= amount) {
            reg.price = 0;
            numRegistrants--;
            emit Refund(_recipient, amount, reg.name);
            _recipient.transfer(amount);
        }
    }
}
